package de.mobilesol.matebet.web.dao;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.google.appengine.api.datastore.Text;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import de.mobilesol.matebet.web.dto.contact.ContactDTO;

public class ContactDAO {
	private static final Logger log = Logger
			.getLogger(ContactDAO.class.getName());
	private static final String TABLE = "contact_t";

	public void updateContacts(int userId, List<ContactDTO> contacts) {
		log.info("updateContacts(" + userId + ", " + contacts.size() + ")");

		DatastoreService datastore = DatastoreServiceFactory
				.getDatastoreService();

		Key key = KeyFactory.createKey(TABLE, userId);
		Entity e = new Entity(key);
		e.setProperty("userId", userId);

		Gson g = new Gson();
		String json = g.toJson(contacts);
		e.setProperty("contacts", new Text(json));
		e.setProperty("modified", new Date());

		datastore.put(e);
	}

	public List<ContactDTO> getContacts(int userId) {
		log.info("getContacts(" + userId + ")");

		DatastoreService datastore = DatastoreServiceFactory
				.getDatastoreService();

		Query q = new Query(TABLE);
		q.setFilter(
				new FilterPredicate("userId", FilterOperator.EQUAL, userId));
		PreparedQuery pq = datastore.prepare(q);

		Iterator<Entity> it = pq.asIterator();
		if (it.hasNext()) {
			Text t = (Text) it.next().getProperty("contacts");
			if (t != null) {
				Gson gson = new Gson();
				Type ct = new TypeToken<List<ContactDTO>>() {
				}.getType();
				List<ContactDTO> l = gson.fromJson(t.getValue(), ct);
				return l;
			}
		}
		return new ArrayList<ContactDTO>();
	}
}
