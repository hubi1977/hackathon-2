package de.mobilesol.matebet.web.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;

public class FriendRequestDAO {
	private static final Logger log = Logger
			.getLogger(FriendRequestDAO.class.getName());
	private static final String TABLE = "user_x_request";

	public void addFriendRequest(int userId, int friendId) {
		log.info("addFriendRequest(" + userId + ", " + friendId + ")");

		DatastoreService datastore = DatastoreServiceFactory
				.getDatastoreService();

		Key key = KeyFactory.createKey(TABLE, userId + "," + friendId);
		Entity e = new Entity(key);
		e.setProperty("userId", userId);
		e.setProperty("friendId", friendId);
		e.setProperty("created", new Date());

		datastore.put(e);
	}

	public void removeFriendRequest(int userId, int friendId) {
		log.info("removeFriendRequest(" + userId + ", " + friendId + ")");

		DatastoreService datastore = DatastoreServiceFactory
				.getDatastoreService();

		Key key = KeyFactory.createKey(TABLE, userId + "," + friendId);
		datastore.delete(key);
	}

	public List<Integer> getFriendRequests(int userId) {
		log.info("getFriends(" + userId + ")");

		DatastoreService datastore = DatastoreServiceFactory
				.getDatastoreService();

		List<Integer> friends = new ArrayList<Integer>();
		Query q = new Query(TABLE);
		q.setFilter(
				new FilterPredicate("friendId", FilterOperator.EQUAL, userId));
		PreparedQuery pq = datastore.prepare(q);

		Iterator<Entity> it = pq.asIterator();
		while (it.hasNext()) {
			Number n = (Number) it.next().getProperty("userId");
			friends.add(n.intValue());
		}

		return friends;
	}

	public Set<Integer> getFriendRequestsByUser(int userId) {
		log.info("getFriendRequestsByUser(" + userId + ")");

		DatastoreService datastore = DatastoreServiceFactory
				.getDatastoreService();

		Set<Integer> friends = new HashSet<Integer>();
		Query q = new Query(TABLE);
		q.setFilter(
				new FilterPredicate("userId", FilterOperator.EQUAL, userId));
		PreparedQuery pq = datastore.prepare(q);

		Iterator<Entity> it = pq.asIterator();
		while (it.hasNext()) {
			Number n = (Number) it.next().getProperty("friendId");
			friends.add(n.intValue());
		}

		return friends;
	}
}
