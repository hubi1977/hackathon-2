package de.mobilesol.matebet.web.dao;

import java.util.ConcurrentModificationException;
import java.util.Random;

import org.apache.log4j.Logger;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Transaction;

/**
 * This initial implementation simply counts all instances of the
 * SimpleCounterShard kind in the datastore. The only way to increment the
 * number of shards is to add another shard by creating another entity in the
 * datastore.
 */
public class ShardedCounter {

	/**
	 * DatastoreService object for Datastore access.
	 */
	private static final DatastoreService DS = DatastoreServiceFactory
			.getDatastoreService();

	/**
	 * Default number of shards.
	 */
	private static final int NUM_SHARDS = 20;

	/**
	 * A random number generator, for distributing writes across shards.
	 */
	private final Random generator = new Random();

	/**
	 * A logger object.
	 */
	private static final Logger LOG = Logger
			.getLogger(ShardedCounter.class.getName());

	private final String name;
	private final int initSize;

	public ShardedCounter(String name, int init) {
		this.name = name;
		this.initSize = init;
	}

	/**
	 * Retrieve the value of this sharded counter.
	 *
	 * @return Summed total of all shards' counts
	 */
	public final int getCount() {
		int sum = 0;

		Query query = new Query(name);
		for (Entity e : DS.prepare(query).asIterable()) {
			sum += ((Number) e.getProperty("count")).intValue();
		}

		return sum + initSize;
	}

	/**
	 * Increment the value of this sharded counter.
	 */
	public final void increment() {
		int shardNum = generator.nextInt(NUM_SHARDS);
		Key shardKey = KeyFactory.createKey(name, Integer.toString(shardNum));

		Transaction tx = DS.beginTransaction();
		Entity shard;
		try {
			try {
				shard = DS.get(tx, shardKey);
				int count = ((Number) shard.getProperty("count")).intValue();
				shard.setUnindexedProperty("count", count + 1L);
			} catch (EntityNotFoundException e) {
				shard = new Entity(shardKey);
				shard.setUnindexedProperty("count", 1);
			}
			DS.put(tx, shard);
			tx.commit();
		} catch (ConcurrentModificationException e) {
			LOG.warn("You may need more shards. Consider adding more shards.");
			LOG.warn(e.toString(), e);
		} catch (Exception e) {
			LOG.warn(e.toString(), e);
		} finally {
			if (tx.isActive()) {
				tx.rollback();
			}
		}
	}
}