package de.mobilesol.matebet.web.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;

import de.mobilesol.matebet.web.dto.TeamDTO;

public class TeamDAO {
	private static final Logger log = Logger.getLogger(TeamDAO.class.getName());
	private static final String TABLE = "team_t";

	public int addTeam(TeamDTO a) {
		log.info("addTeam" + a);

		DatastoreService datastore = DatastoreServiceFactory
				.getDatastoreService();

		Key key = KeyFactory.createKey(TABLE, a.teamId);
		try {
			datastore.get(key);
			throw new IllegalStateException(
					"id " + a.teamId + " already exists,");
		} catch (EntityNotFoundException e1) {
			Entity e = toEntity(new Entity(key), a);
			e.setProperty("created", new Date());

			datastore.put(e);

			return a.teamId;
		}
	}

	public TeamDTO getTeam(int id) {

		DatastoreService datastore = DatastoreServiceFactory
				.getDatastoreService();

		Query q = new Query(TABLE);
		q.setFilter(new FilterPredicate("teamId", FilterOperator.EQUAL, id));

		PreparedQuery pq = datastore.prepare(q);

		Iterator<Entity> it = pq.asIterator();
		if (it.hasNext()) {
			TeamDTO a = toObject(it.next());
			return a;
		}

		return null;
	}

	public List<TeamDTO> getAllTeams() {

		DatastoreService datastore = DatastoreServiceFactory
				.getDatastoreService();

		Query q = new Query(TABLE);
		PreparedQuery pq = datastore.prepare(q);
		List<TeamDTO> l = new ArrayList<TeamDTO>();
		Iterator<Entity> it = pq.asIterator();
		while (it.hasNext()) {
			TeamDTO a = toObject(it.next());
			l.add(a);
		}

		return l;
	}

	private Entity toEntity(Entity e, TeamDTO a) {
		e.setProperty("teamId", a.teamId);
		e.setProperty("name", a.name);

		return e;
	}

	private TeamDTO toObject(Entity e) {
		TeamDTO a = new TeamDTO();
		a.name = (String) e.getProperty("name");
		a.teamId = ((Number) e.getProperty("teamId")).intValue();

		return a;
	}
}
