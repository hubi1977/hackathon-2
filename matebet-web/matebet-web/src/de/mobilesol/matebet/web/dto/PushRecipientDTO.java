package de.mobilesol.matebet.web.dto;

import java.io.Serializable;

public class PushRecipientDTO implements Serializable {

	private static final long serialVersionUID = -4659975042026914814L;

	public String token;
	public int userId;
	public DeviceDTO device;

	@Override
	public String toString() {
		return "PushDTO [token=" + token + ", userId=" + userId + ", device="
				+ device + "]";
	}

	public static class DeviceDTO implements Serializable {
		private static final long serialVersionUID = 1L;
		public String platform, version, uuid, cordova, model, manufacturer,
				serial;

		@Override
		public String toString() {
			return "DeviceDTO [platform=" + platform + ", version=" + version
					+ ", uuid=" + uuid + ", cordova=" + cordova + ", model="
					+ model + ", manufacturer=" + manufacturer + ", serial="
					+ serial + "]";
		}
	}
}
