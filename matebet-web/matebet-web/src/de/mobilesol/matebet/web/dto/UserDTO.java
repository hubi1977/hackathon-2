package de.mobilesol.matebet.web.dto;

import java.io.Serializable;
import java.util.Date;

public class UserDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	public int userId;
	public String name, email, password, picture;

	public String extId;

	// used for update
	public String oldpassword;

	public String age;
	public boolean extAuth;

	public Date created;

	@Override
	public String toString() {
		return "UserDTO [id=" + userId + ", name=" + name + ", email=" + email
				+ "]";
	}

}
