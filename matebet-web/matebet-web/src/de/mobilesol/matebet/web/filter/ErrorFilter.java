package de.mobilesol.matebet.web.filter;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Date;
import java.util.Enumeration;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.google.apphosting.api.DeadlineExceededException;

import de.mobilesol.matebet.web.exception.AuthException;
import de.mobilesol.matebet.web.exception.MatebetException;
import de.mobilesol.matebet.web.exception.MatebetValidationException;
import de.mobilesol.matebet.web.exception.ResourceNotFoundException;
import de.mobilesol.matebet.web.util.SendMail;

public class ErrorFilter implements Filter {

	private static final Logger log = Logger
			.getLogger(ErrorFilter.class.getName());

	@Override
	public void destroy() {
	}

	@Override
	public void doFilter(ServletRequest arg0, ServletResponse arg1,
			FilterChain arg2) throws IOException, ServletException {

		long s = System.currentTimeMillis();
		HttpServletRequest req = (HttpServletRequest) arg0;
		HttpServletResponse res = (HttpServletResponse) arg1;
		log.debug("before request " + req.getRequestURI());
		try {
			arg2.doFilter(req, res);
			int t = (int) (System.currentTimeMillis() - s);
			if (t < 2000) {
				log.debug("successfully finished request in " + t + "ms");
			} else if (t < 5000 || req.getRequestURI().startsWith("/_ah/")
					|| req.getRequestURI().startsWith("/admin/")
					|| req.getRequestURI().startsWith("/cron/")) {
				log.info("successfully finished request in " + t + "ms");
			} else {
				log.warn("successfully finished request in " + t + "ms");
			}
		} catch (MatebetValidationException e) {
			log.warn("after request" + e.code + "; " + e.message);
			log.warn(e.getMessage() + "; code=" + e.code + "; json="
					+ e.getJson(), e);

			res.setContentType("application/json");
			res.getWriter().append(e.getJson());
			res.setStatus(400);
		} catch (ResourceNotFoundException e) {
			log.warn("after request" + e.code + "; " + e.message);
			log.warn(e.getMessage() + "; code=" + e.code, e);

			res.setContentType("application/json");
			res.getWriter().append(e.getJson());
			res.setStatus(404);
		} catch (AuthException e) {
			log.warn("after request" + e.code + "; " + e.message);
			log.warn(e.getMessage() + "; code=" + e.code, e);

			res.setContentType("application/json");
			res.getWriter().append(e.getJson());
			res.setStatus(401);
		} catch (MatebetException e) {
			log.warn("after request" + e.code + "; " + e.message);
			log.warn(e.getMessage() + "; code=" + e.code, e);

			res.setContentType("application/json");
			res.getWriter().append(e.getJson());
			res.setStatus(400);
		} catch (IOException e) {
			log.error("*********** after request", e);
			new SendMail().send("Error on matebet.de",
					"<p>failed</p><pre>" + txt(req, e) + "</pre>",
					"lui.baeumer@gmail.com", "lui.baeumer@gmail.com", null);
			throw e; // res.sendRedirect("/500.jsp");
		} catch (ServletException e) {
			log.error("*********** after request", e);
			new SendMail().send("Error on matebet.de",
					"<p>failed</p><pre>" + txt(req, e) + "</pre>",
					"lui.baeumer@gmail.com", "lui.baeumer@gmail.com", null);
			throw e; // res.sendRedirect("/500.jsp");
		} catch (DeadlineExceededException e) {
			log.error("*********** after request", e);
			new SendMail().send("DeadlineExceeded on matebet.de",
					"Deadline Exceeded!!!\n " + txt(req, e),
					"lui.baeumer@gmail.com", "lui.baeumer@gmail.com", null);
			throw e; // res.sendRedirect("/500.jsp");
		} catch (RuntimeException e) {
			log.error("*********** after request", e);
			new SendMail().send("Error on matebet.de",
					"<p>failed</p><pre>" + txt(req, e) + "</pre>",
					"lui.baeumer@gmail.com", "lui.baeumer@gmail.com", null);
			throw e; // res.sendRedirect("/500.jsp");
		} catch (Error e) {
			log.error("*********** after request", e);
			new SendMail().send("Error on matebet.de",
					"<p>failed</p><pre>" + txt(req, e) + "</pre>",
					"lui.baeumer@gmail.com", "lui.baeumer@gmail.com", null);
			throw e; // res.sendRedirect("/500.jsp");
		}
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
	}

	private String txt(ServletRequest req, Throwable t) {
		StringWriter s = new StringWriter();
		PrintWriter p = new PrintWriter(s, true);
		t.printStackTrace(p);
		p.close();

		StringBuffer data = new StringBuffer("date=" + new Date() + "\n");
		if (req instanceof HttpServletRequest) {
			HttpServletRequest r = (HttpServletRequest) req;
			data.append("contextPath=" + r.getContextPath() + "\n");
			data.append("reqestUrl=" + r.getRequestURL() + "\n");
			data.append("reqestUri=" + r.getRequestURI() + "\n");
			data.append("queryString=" + r.getQueryString() + "\n");
			data.append("uri=" + r.getRequestURI() + "\n");
			@SuppressWarnings("rawtypes")
			Enumeration e = r.getHeaderNames();
			while (e.hasMoreElements()) {
				String h = e.nextElement().toString();
				String v = r.getHeader(h);
				data.append(h + "=" + v + "\n");
			}
		}
		data.append("remote=" + req.getRemoteAddr() + "/" + req.getRemoteHost()
				+ ":" + req.getRemotePort() + "\n");

		String ret = data + s.getBuffer().toString();
		return ret;
	}
}
