package de.mobilesol.matebet.web.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.cache.Cache;

import org.apache.log4j.Logger;

import de.mobilesol.matebet.web.dao.MatchTipDAO;
import de.mobilesol.matebet.web.dto.BetDTO;
import de.mobilesol.matebet.web.dto.BetDTO.BetStatus;
import de.mobilesol.matebet.web.dto.BetGroupDTO;
import de.mobilesol.matebet.web.dto.BetMatchDTO;
import de.mobilesol.matebet.web.dto.MatchTipDTO;
import de.mobilesol.matebet.web.dto.MatchTipDTO.TipDTO;
import de.mobilesol.matebet.web.dto.MatchTipDTO.TipsByMatchByGroupMap;
import de.mobilesol.matebet.web.dto.MatchTipDTO.TipsByMatchMap;
import de.mobilesol.matebet.web.dto.PushNotificationDTO;
import de.mobilesol.matebet.web.dto.PushNotificationDTO.Page;
import de.mobilesol.matebet.web.dto.UserDTO;
import de.mobilesol.matebet.web.exception.MatebetException;
import de.mobilesol.matebet.web.exception.MatebetValidationException;
import de.mobilesol.matebet.web.service.master.BetService;
import de.mobilesol.matebet.web.util.Util;

public class MatchTipService {
	private static final Logger log = Logger
			.getLogger(MatchTipService.class.getName());

	private MatchTipDAO dao = new MatchTipDAO();

	private BetService betService = new BetService();
	private PushService pushService = new PushService();
	private UserService userService = new UserService();

	public void addMatchTips(int betId, int groupId, int userId,
			TipsByMatchMap a) {

		log.info("addMatchTips(" + betId + ", " + groupId + ", " + userId + ", "
				+ a + ")");

		String key = "mb:matchtips:" + betId + ":" + userId;
		Util.getCache().remove(key);
		key = "mb:matchtips:" + betId + ":" + -1;
		Util.getCache().remove(key);

		final BetDTO bet = betService.getBet(betId);
		validate(bet, userId, a);
		MatchTipDTO oldTips = dao.getMatchTips(betId, groupId, userId);
		TipsByMatchByGroupMap oldMap = oldTips.groupsByUser.get(userId);
		TipsByMatchMap oldMap2 = null;
		if (oldMap != null) {
			oldMap2 = oldMap.groups.get(groupId);
			if (oldMap2 == null) {
				oldMap2 = new TipsByMatchMap();
				oldMap.groups.put(groupId, oldMap2);
			}
			log.info("found old tips" + oldMap2 + ", so add new " + a.tips);
			oldMap2.tips.putAll(a.tips);
		} else {
			// user has not tipped any other matches
			oldMap2 = a;
			log.info("add new " + a.tips);
		}
		dao.addMatchTips(betId, groupId, userId, oldMap2);

		// final int opponentId = (bet.userId == userId ? bet.opponentId
		// : bet.userId);

		// check if user has entered all tips
		Set<Integer> nonFinishedMatches = new HashSet<Integer>();
		for (BetGroupDTO m : bet.groups) {
			log.info("group" + m.groupId);
			for (BetMatchDTO bm : m.matches) {
				log.info("match" + bm.matchId + ";" + bm.matchdate);
				if (new Date().before(bm.matchdate)) {
					nonFinishedMatches.add(bm.matchId);
				}
			}
		}

		log.info("#nonFinished matches=" + nonFinishedMatches.size() + ";"
				+ nonFinishedMatches);

		MatchTipDTO userMatchTips = dao.getMatchTips(betId, -1, -1);
		log.info("userMatchTips=" + userMatchTips);

		// tips may not be written by gae, so fix userMatchTips
		oldMap = userMatchTips.groupsByUser.get(userId);
		if (oldMap == null) {
			oldMap = new TipsByMatchByGroupMap();
			oldMap.groups = new HashMap<Integer, TipsByMatchMap>();
			userMatchTips.groupsByUser.put(userId, oldMap);
		}
		oldMap.groups.put(groupId, oldMap2);

		// now check if all matches have been tipped
		List<Boolean> userFinishedList = new ArrayList<Boolean>();

		for (Integer u : bet.userIds) {
			Set<Integer> userMatchTipSet = coveredMatches(
					userMatchTips.groupsByUser.get(u));
			log.info("user " + u + " covered " + userMatchTips);

			if (userId == u) {
				// if db is not fast enough
				userMatchTipSet.addAll(a.tips.keySet());
			}

			boolean userFinished = (userMatchTipSet
					.containsAll(nonFinishedMatches));
			userFinishedList.add(userFinished);
		}

		log.info("userFinished=" + userFinishedList);

		boolean allFinished = true;
		for (boolean f : userFinishedList) {
			if (!f) {
				allFinished = false;
			}
		}

		if (allFinished) {
			// have all users finished the bet?
			log.info("all matches have been tipped. So close bet " + betId
					+ "; userMatchTips" + userMatchTips);
		}

		report(userId, bet.userIds, userFinishedList, allFinished);
	}

	private void report(int userId, List<Integer> userIds,
			List<Boolean> userFinishedList, boolean allFinished) {

		UserDTO user = userService.getUser(userId, false);
		Boolean userFinished = null;
		for (int i = 0; i < userIds.size(); i++) {
			if (userId == userIds.get(i)) {
				userFinished = userFinishedList.get(i);
			}
		}

		for (int i = 0; i < userIds.size(); i++) {
			int opponentId = userIds.get(i);
			if (userId != opponentId) {

				boolean opponentFinished = userFinishedList.get(i);

				Map<String, Object> payload = new HashMap<String, Object>();

				// send push to opponent
				if (!opponentFinished && !userFinished) {
					pushService.send(opponentId,
							new PushNotificationDTO("Du bist dran!",
									user.name + " hat weitere Spiele getippt.",
									Page.OPEN_BETS, payload));
				} else if (!opponentFinished && userFinished) {
					pushService.send(opponentId,
							new PushNotificationDTO("Du bist dran!",
									user.name + " hat alle Spiele getippt.",
									Page.OPEN_BETS, payload));
				} else if (allFinished) {
					pushService.send(opponentId,
							new PushNotificationDTO("Wette abgeschlossen.",
									user.name + " hat alle Spiele getippt.",
									Page.OPEN_BETS, payload));
				}
			}
		}
	}

	private Set<Integer> coveredMatches(TipsByMatchByGroupMap m) {
		Set<Integer> matches = new HashSet<Integer>();

		if (m != null && m.groups != null) {
			for (Map.Entry<Integer, TipsByMatchMap> entry : m.groups
					.entrySet()) {
				TipsByMatchMap tbmm = entry.getValue();
				if (tbmm.tips != null) {
					for (Map.Entry<Integer, TipDTO> e1 : tbmm.tips.entrySet()) {
						if (e1.getValue() != null) {
							matches.add(e1.getKey());
						}
					}
				}
			}
		}

		return matches;
	}

	@SuppressWarnings("unchecked")
	public MatchTipDTO getMatchTips(int betId, int userId) {
		log.info("getMatchTips(" + betId + "," + userId + ")");

		final String key = "mb:matchtips:" + betId + ":" + userId;

		Cache c = null;
		MatchTipDTO o = null;
		try {
			c = Util.getCache();
			o = (MatchTipDTO) c.get(key);
			if (o == null) {
				log.info("reading from db");
				o = dao.getMatchTips(betId, -1, userId);
				c.put(key, o);
			}
		} catch (Exception e) {
			log.error("failed", e);
			o = dao.getMatchTips(betId, -1, userId);
		}

		return o;
	}

	private void validate(BetDTO bet, int userId, TipsByMatchMap a) {
		if (BetStatus.CLOSED.equals(bet.status)
				|| BetStatus.FINISHED.equals(bet.status)) {
			throw new MatebetValidationException(
					MatebetException.EXC_BET_ALREADY_CLOSED,
					"Bet " + bet + " already closed ");
		}
		if (BetStatus.INITIATED.equals(bet.status)) {
			throw new MatebetValidationException(
					MatebetException.EXC_BET_NOT_STARTED,
					"Bet " + bet + " is not accepted");
		}
		boolean found = false;
		for (Integer u : bet.userIds) {
			if (u == null) {
				throw new MatebetValidationException(302,
						"Bet " + bet + " contains null-users " + bet.userIds);
			}
			if (userId == u) {
				found = true;
			}
		}
		if (!found) {
			throw new MatebetValidationException(302,
					"Bet " + bet + " is not valid user " + userId);
		}
		if (a == null || a.tips == null || a.tips.isEmpty()) {
			throw new MatebetValidationException(303,
					"Tip for bet " + bet + " is null; user " + userId);
		}
		for (TipDTO tip : a.tips.values()) {
			if (tip == null) {
				throw new MatebetValidationException(303,
						"Tip for bet " + bet + " is illegal: " + tip);
			}
		}
	}
}
