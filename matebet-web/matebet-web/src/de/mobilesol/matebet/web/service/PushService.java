package de.mobilesol.matebet.web.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import de.mobilesol.matebet.web.dao.PushRecipientDAO;
import de.mobilesol.matebet.web.dto.PushRecipientDTO;
import de.mobilesol.matebet.web.dto.PushNotificationDTO;
import de.mobilesol.matebet.web.service.push.PushTransmitterAndroid;
import de.mobilesol.matebet.web.service.push.PushTransmitterIos;

public class PushService {

	private static final Logger log = Logger
			.getLogger(PushService.class.getName());

	private PushRecipientDAO dao = new PushRecipientDAO();

	public PushRecipientDTO registerPush(PushRecipientDTO push)
			throws IOException {
		log.info("push=" + push);

		dao.addPush(push);

		return push;
	}

	public String unregisterPush(int userId, String uuid) throws IOException {
		log.info("user=" + userId + ";" + uuid);
		dao.removePush(userId, uuid);
		return uuid;
	}

	public void unregisterPushByToken(int userId, String token)
			throws IOException {
		log.info("removePushByToken(" + userId + ";" + token + ")");
		dao.removePushByToken(userId, token);
	}

	public Object send(int userId, PushNotificationDTO push) {

		List<PushRecipientDTO> list = dao.getPush(userId);

		log.info("send to " + userId + " (" + list.size() + " reg IDs)");
		if (list.isEmpty()) {
			log.error("did not find any tokens for " + userId);
			return null;
		}

		List<PushRecipientDTO> androidPushes = new ArrayList<PushRecipientDTO>();
		List<PushRecipientDTO> iosPushes = new ArrayList<PushRecipientDTO>();

		for (PushRecipientDTO p : list) {
			if (p.device != null && p.device.platform != null
					&& p.device.platform.equals("iOS")) {
				iosPushes.add(p);
			} else if (p.device != null && p.device.platform != null
					&& p.device.platform.equals("Android")) {
				androidPushes.add(p);
			} else {
				log.warn("unknown device: " + p.device + ". So assume Android");
				// TODO: report this
				androidPushes.add(p);
			}
		}

		log.info("sending " + iosPushes.size() + " ios pushes and "
				+ androidPushes.size() + " android pushes");

		Map<String, Object> ret = new HashMap<String, Object>();

		// sending android pushes
		Object androidRes = new PushTransmitterAndroid().sendPush(androidPushes,
				push);

		// sending ios pushes
		Object iosRes = new PushTransmitterIos().sendPush(iosPushes, push);

		if (androidRes != null) {
			ret.put("Android", androidRes);
		}
		if (iosRes != null) {
			ret.put("Ios", iosRes);
		}

		return ret;
	}
}
