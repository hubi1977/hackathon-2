package de.mobilesol.matebet.web.service.facebook.dto;

import java.util.Map;

public class FacebookUserDTO {
	public String id;
	public String name, email;
	public MapFb picture;
	public AgeRangeFb age_range;

	public static class MapFb {
		public Map<String, String> data;
	}

	public static class AgeRangeFb {
		public Integer min, max;
	}

	@Override
	public String toString() {
		return "UserFb [id=" + id + ", name=" + name + ", email=" + email
				+ ", picture=" + picture + "]";
	}
}
