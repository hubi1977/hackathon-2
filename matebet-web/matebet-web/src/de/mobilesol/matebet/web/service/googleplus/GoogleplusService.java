package de.mobilesol.matebet.web.service.googleplus;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.google.gson.Gson;

import de.mobilesol.matebet.web.dto.UserDTO;
import de.mobilesol.matebet.web.service.UserService;
import de.mobilesol.matebet.web.service.googleplus.dto.GoogleplusFriendDTO;
import de.mobilesol.matebet.web.service.googleplus.dto.GoogleplusFriendDTO.GpDTO;
import de.mobilesol.matebet.web.service.googleplus.dto.GoogleplusUserDTO;
import de.mobilesol.matebet.ws.dto.UserProfileDTO;

public class GoogleplusService {
	private static final Logger log = Logger
			.getLogger(GoogleplusService.class.getName());

	private UserService userService = new UserService();

	public UserProfileDTO readProfile(String accessToken) {
		log.info("readProfile(" + accessToken + ")");
		URL url;
		try {
			url = new URL(
					"https://www.googleapis.com/plus/v1/people/me?access_token="
							+ accessToken);
			InputStream in = url.openStream();
			BufferedReader reader = new BufferedReader(
					new InputStreamReader(in));
			try {
				String line;
				StringBuffer strb = new StringBuffer();
				while ((line = reader.readLine()) != null) {
					strb.append(line + "\n");
				}

				log.info("stb=" + strb);

				GoogleplusUserDTO ufb = new Gson().fromJson(strb.toString(),
						GoogleplusUserDTO.class);

				UserProfileDTO profile = new UserProfileDTO();
				profile.id = "gp:" + ufb.id;
				if (ufb.emails != null && !ufb.emails.isEmpty()) {
					profile.email = ufb.emails.get(0).get("value");
				}
				profile.name = ufb.displayName;
				profile.picture = (ufb.image != null
						? ufb.image.get("url")
						: null);
				if (ufb.ageRange != null && ufb.ageRange.min != null
						&& ufb.ageRange.min >= 18) {
					profile.age = "c";
				} else if (ufb.ageRange != null && ufb.ageRange.min != null
						&& ufb.ageRange.min >= 16) {
					profile.age = "b";
				} else if (ufb.ageRange == null || ufb.ageRange.min == null) {
					profile.age = "0";
				} else {
					profile.age = "a";
				}

				return profile;
			} finally {
				reader.close();
			}
		} catch (IOException e) {
			log.warn("readProfile failed", e);
			e.printStackTrace();
			return null;
		}
	}

	public List<UserDTO> getFriends(String accessToken) {
		URL url;
		List<UserDTO> userFriends = new ArrayList<UserDTO>();

		try {
			url = new URL(
					"https://www.googleapis.com/plus/v1/people/me/people/visible?access_token="
							+ accessToken);
			InputStream in = url.openStream();
			BufferedReader reader = new BufferedReader(
					new InputStreamReader(in, "UTF-8"));
			try {
				String line;
				StringBuffer strb = new StringBuffer();
				while ((line = reader.readLine()) != null) {
					strb.append(line + "\n");
				}

				log.info("stb=" + strb);
				Gson gson = new Gson();
				GoogleplusFriendDTO user = gson.fromJson(strb.toString(),
						GoogleplusFriendDTO.class);

				for (GpDTO ff : user.items) {
					UserDTO uf = userService.getUserByName(ff.displayName);
					if (uf != null) {
						userFriends.add(uf);
					}
				}

				log.info("found users=" + userFriends);
			} finally {
				reader.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		return userFriends;
	}
}
