package de.mobilesol.matebet.web.service.master;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.cache.Cache;

import org.apache.log4j.Logger;

import com.google.appengine.api.datastore.EntityNotFoundException;

import de.mobilesol.matebet.web.dao.BetDAO;
import de.mobilesol.matebet.web.dto.BetDTO;
import de.mobilesol.matebet.web.dto.BetDTO.BetStatus;
import de.mobilesol.matebet.web.dto.BetGroupDTO;
import de.mobilesol.matebet.web.dto.BetMatchDTO;
import de.mobilesol.matebet.web.dto.MatchDTO;
import de.mobilesol.matebet.web.dto.MatchTipDTO;
import de.mobilesol.matebet.web.dto.PushNotificationDTO;
import de.mobilesol.matebet.web.dto.PushNotificationDTO.Page;
import de.mobilesol.matebet.web.dto.TeamDTO;
import de.mobilesol.matebet.web.dto.UserDTO;
import de.mobilesol.matebet.web.exception.MatebetValidationException;
import de.mobilesol.matebet.web.service.PushService;
import de.mobilesol.matebet.web.service.UserService;
import de.mobilesol.matebet.web.util.Util;

public class BetService {
	private static final Logger log = Logger
			.getLogger(BetService.class.getName());

	private BetDAO dao = new BetDAO();

	private MatchService matchService = new MatchService();
	private GroupService groupService = new GroupService();
	private LeagueService leagueService = new LeagueService();
	private UserService userService = new UserService();
	private PushService pushService = new PushService();

	public int addBet(BetDTO a) {
		log.info("addBet" + a);
		validate(a);

		if (a.userNames == null) {
			a.userNames = new HashMap<Integer, String>();
			for (Integer userId : a.userIds) {
				UserDTO user = userService.getUser(userId, false);
				if (user == null) {
					throw new MatebetValidationException(110,
							"user does not exist." + a);
				}
				a.userNames.put(userId, user.name);
			}
		}
		a.leagueName = leagueService.getLeague(a.leagueId).title;

		long lastBetDate = 0;
		List<BetGroupDTO> groups = new ArrayList<BetGroupDTO>();

		// loop over groups and set date
		for (BetGroupDTO group : a.groups) {
			if (group.name == null) {
				int gid = group.groupId;
				group.name = groupService.getGroup(gid).name;
			}
			long lastMatchDate = 0;
			long firstMatchDate = Long.MAX_VALUE;

			List<MatchDTO> matches = matchService
					.getMatchesByGroup(group.groupId);
			Map<Integer, MatchDTO> map = new HashMap<Integer, MatchDTO>();
			for (MatchDTO m : matches) {
				map.put(m.matchId, m);
			}

			for (BetMatchDTO bm : group.matches) {
				bm.matchdate = new Date();
				if (lastMatchDate < bm.matchdate.getTime()) {
					lastMatchDate = bm.matchdate.getTime();
				}
				if (firstMatchDate > bm.matchdate.getTime()) {
					firstMatchDate = bm.matchdate.getTime();
				}
				if (lastBetDate < bm.matchdate.getTime()) {
					lastBetDate = bm.matchdate.getTime();
				}

				bm.team1 = new TeamDTO();
				bm.team1.teamId = 1;
				bm.team2 = new TeamDTO();
				bm.team2.teamId = 2;
			}

			group.lastMatchdate = new Date(lastMatchDate);
			group.firstMatchdate = new Date(firstMatchDate);

			if (group.matches.size() > 0) {
				groups.add(group);
			}
		}

		if (groups.size() == 0) {
			throw new MatebetValidationException(111,
					"there are not groups contained");
		}
		a.groups = groups;
		a.lastMatchdate = new Date(lastBetDate);
		a.confirmedBy = new HashMap<Integer, Boolean>();
		a.confirmedBy.put(a.userIds.get(0), true);

		int id = dao.addBet(a);

		for (int userId : a.userIds) {
			Util.getCache().remove("mb:bet:user:" + userId);
		}

		Map<String, Object> payload = new HashMap<String, Object>();
		payload.put("betId", Integer.toString(id));
		for (int i = 1; i < a.userIds.size(); i++) {
			pushService.send(a.userIds.get(i),
					new PushNotificationDTO(
							a.userNames.get(a.userIds.get(0))
									+ " fordert dich heraus.",
							"Möchtest du die Herausforderung annehmen?",
							Page.OPEN_BETS, payload));
		}

		log.info("created bet with id " + id);

		return id;
	}

	public List<BetDTO> getOpenBets(int userId) {

		List<BetDTO> bets;
		try {
				bets = dao.getOpenBets(userId);
		} catch (Exception e) {
			log.error("failed", e);
			bets = dao.getOpenBets(userId);
		}

		return bets;
	}

	public BetDTO confirmBet(int betId, int opponentId) {
		log.info("confirmBet" + betId + "," + opponentId);
		try {
			BetDTO bet = dao.confirmBet(betId, opponentId, true);

			if (bet == null) {
				throw new MatebetValidationException(115,
						"Bet has already been confirmed");
			}

			Util.getCache().remove("mb:bet:" + betId);
			for (int userId : bet.userIds) {
				Util.getCache().remove("mb:bet:user:" + userId);
			}

			UserDTO user = userService.getUser(bet.userIds.get(0), false);
			UserDTO opponent = userService.getUser(opponentId, false);

			Map<String, Object> payload = new HashMap<String, Object>();
			payload.put("betId", Integer.toString(bet.betId));
			pushService.send(user.userId,
					new PushNotificationDTO(opponent.name + " hat angenommen.",
							opponent.name + " hat deine Wette angenommen.",
							Page.OPEN_BETS, payload));

			return bet;
		} catch (EntityNotFoundException e) {
			log.warn("e=" + e);
			return null;
		}
	}

	public BetDTO declineBet(int betId) {
		BetDTO bet = dao.declineBet(betId);

		Util.getCache().remove("mb:bet:" + betId);
		if (bet != null) {
			for (int userId : bet.userIds) {
				Util.getCache().remove("mb:bet:user:" + userId);
			}
		}

		return bet;
	}

	public void deleteBet(int betId) {
		dao.deleteBet(betId);
		Util.getCache().remove("mb:bet:" + betId);
	}

	public BetDTO declineBet(int betId, int opponentId) {
		log.info("declineBet(" + betId + "," + opponentId + ")");
		try {
			BetDTO bet = dao.confirmBet(betId, opponentId, false);

			Util.getCache().remove("mb:bet:" + betId);
			for (int userId : bet.userIds) {
				Util.getCache().remove("mb:bet:user:" + userId);
				Util.getCache().remove("mb:closed:" + userId);
				Util.getCache().remove("mb:closed:" + betId + ":" + userId);
			}

			UserDTO user = userService.getUser(bet.userIds.get(0), false);
			UserDTO opponent = userService.getUser(opponentId, false);

			Map<String, Object> payload = new HashMap<String, Object>();
			payload.put("betId", Integer.toString(bet.betId));

			if (BetStatus.RUNNING.name().equals(bet.status.name())) {
				pushService.send(user.userId,
						new PushNotificationDTO(
								opponent.name + " hat abgelehnt.",
								opponent.name + " hat deine Wette abgelehnt.",
								Page.OPEN_BETS, payload));
			} else {
				pushService.send(user.userId,
						new PushNotificationDTO(
								opponent.name + " hat abgelehnt.",
								opponent.name + " hat deine Wette abgelehnt.",
								Page.CLOSED_BETS, payload));
			}

			return bet;
		} catch (EntityNotFoundException e) {
			log.warn("e=" + e);
			return null;
		}
	}

	public BetDTO getBet(int betId) {
		log.info("getBet(" + betId + ")");

		final String key = "mb:bet:" + betId;
		Cache c = null;
		BetDTO bet;
		try {
			c = Util.getCache();
			bet = (BetDTO) c.get(key);
			if (bet == null) {
				log.info("reading from db");
				bet = dao.getBet(betId);
				if (bet == null) {
					throw new MatebetValidationException(112, "bet is null");
				}

				c.put(key, bet);
			} else {
				log.info("reading from cache");
			}
		} catch (MatebetValidationException e) {
			throw e;
		} catch (Exception e) {
			log.error("failed", e);
			bet = dao.getBet(betId);
		}

		return bet;
	}

	public List<BetDTO> getBetsToBeClosed() {
		return dao.getBetsToBeClosed();
	}

	public List<BetDTO> getBetsToBeDeclined() {
		return dao.getBetsToBeDeclined();
	}

	public List<BetDTO> getBetsByStatus(BetStatus status) {
		log.info("getBetsByStatus(" + status + ")");
		List<BetDTO> l = dao.getBetsByStatus(status);
		return l;
	}

	public List<BetDTO> getBetsByLeague(int leagueId) {
		return dao.getBetsByLeague(leagueId);
	}

	public List<BetDTO> deleteBetsByLeague(int id) {
		List<BetDTO> l = dao.deleteBetsByLeague(id);

		Set<Integer> users = new HashSet<Integer>();
		Set<Integer> bets = new HashSet<Integer>();
		for (BetDTO b : l) {
			bets.add(b.betId);
			for (int userId : b.userIds) {
				users.add(userId);
			}
		}

		for (int betId : bets) {
			Util.getCache().remove("mb:bet:" + betId);
		}
		for (int userId : users) {
			Util.getCache().remove("mb:bet:user:" + userId);
		}

		return l;
	}

	private void validate(BetDTO a) {
		if (a == null) {
			throw new MatebetValidationException(112, "bet is null");
		}
		if (a.stake == null) {
			throw new MatebetValidationException(113, "no stake defined");
		}
		if (a.groups == null || a.groups.isEmpty()) {
			throw new MatebetValidationException(114, "no groups defined");
		}
		if (a.userIds == null || a.userIds.size() < 2) {
			throw new MatebetValidationException(115, "no users defined");
		}
		for (Integer userId : a.userIds) {
			if (userId == null) {
				throw new MatebetValidationException(110, "user is null." + a);
			}
		}
		for (BetGroupDTO g : a.groups) {
			if (g.groupId == 0) {
				throw new MatebetValidationException(116,
						"Group does not have an id: " + a);
			}
			if (g.matches == null || g.matches.size() == 0) {
				throw new MatebetValidationException(117,
						"Group does not have matches: " + a);
			}
			for (BetMatchDTO m : g.matches) {
				if (m.matchId == 0) {
					throw new MatebetValidationException(117,
							"Match does not have an id: " + a);
				}
			}
		}
		if (a.title == null) {
			throw new MatebetValidationException(118, "no title defined");
		}
	}
}
