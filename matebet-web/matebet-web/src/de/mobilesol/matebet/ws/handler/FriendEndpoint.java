package de.mobilesol.matebet.ws.handler;

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import de.mobilesol.matebet.web.dto.FriendDTO;
import de.mobilesol.matebet.web.dto.UserDTO;
import de.mobilesol.matebet.web.dto.contact.ContactDTO;
import de.mobilesol.matebet.web.exception.MatebetValidationException;
import de.mobilesol.matebet.web.exception.ResourceNotFoundException;
import de.mobilesol.matebet.web.service.FriendService;

public class FriendEndpoint {

	private static final long serialVersionUID = 1L;

	private static final Logger log = Logger
			.getLogger(FriendEndpoint.class.getName());

	private FriendService friendService = new FriendService();

	public List<FriendDTO> handleGetFriends(int id, HttpServletResponse resp)
			throws IOException {
		List<FriendDTO> u = friendService.getFriends(id);
		if (u == null) {
			throw new ResourceNotFoundException(200,
					"User with id " + id + " not found");
		}

		return u;
	}

	public List<FriendDTO> handleGetFriends(HttpServletResponse resp,
			int userId, String query) throws IOException {
		List<FriendDTO> users = friendService.getFriend(userId, query);
		return users;
	}

	public UserDTO sendInvitationRequest(int userId, int friendId,
			HttpServletResponse resp) {
		if (friendId == 0) {
			throw new ResourceNotFoundException(200, "Friend not set.");
		}
		return friendService.sendInvitationRequest(userId, friendId);
	}

	public List<FriendDTO> acceptInvitationRequest(int userId, FriendDTO friend,
			HttpServletResponse resp) {
		if (friend == null) {
			throw new ResourceNotFoundException(200, "Friend not set.");
		}
		return friendService.acceptInvitationRequest(userId, friend);
	}

	public List<FriendDTO> rejectInvitationRequest(int userId, FriendDTO friend,
			HttpServletResponse resp) {
		if (friend == null) {
			throw new ResourceNotFoundException(200, "Friend not set.");
		}
		return friendService.rejectInvitationRequest(userId, friend);
	}

	public Object handleAddFriend(Reader in, HttpServletResponse resp)
			throws IOException {

		Gson gson = new Gson();
		Friend u = gson.fromJson(in, Friend.class);
		if (u == null) {
			throw new MatebetValidationException(210, "illegal request");
		}

		int userId = u.userId;
		int friendId = u.friendId;

		return friendService.addFriend(userId, friendId, false);
	}

	public Object handleRemoveFriend(int userId, int friendId,
			HttpServletResponse resp) throws IOException {

		return friendService.removeFriend(userId, friendId);
	}

	public List<FriendDTO> handleUpdateContacts(int userId, Reader in,
			HttpServletResponse resp) throws IOException {

		Gson gson = new Gson();
		Type collectionType = new TypeToken<List<ContactDTO>>() {
		}.getType();

		List<ContactDTO> contacts = gson.fromJson(in, collectionType);
		if (contacts == null) {
			throw new MatebetValidationException(212, "illegal request");
		}

		List<FriendDTO> u = friendService.updateContacts(userId, contacts);
		return u;
	}

	private static class Friend implements Serializable {
		private static final long serialVersionUID = 1L;

		public int userId, friendId;
	}
}
