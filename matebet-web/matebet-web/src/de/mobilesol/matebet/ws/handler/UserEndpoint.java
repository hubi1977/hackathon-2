package de.mobilesol.matebet.ws.handler;

import java.io.IOException;
import java.io.Reader;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.google.gson.Gson;

import de.mobilesol.matebet.web.dto.UserDTO;
import de.mobilesol.matebet.web.exception.AuthException;
import de.mobilesol.matebet.web.exception.MatebetException;
import de.mobilesol.matebet.web.exception.ResourceNotFoundException;
import de.mobilesol.matebet.web.service.FriendService;
import de.mobilesol.matebet.web.service.UserService;
import de.mobilesol.matebet.web.service.facebook.FacebookService;
import de.mobilesol.matebet.web.service.facebook.dto.FacebookAccessTokenDTO;
import de.mobilesol.matebet.web.service.googleplus.GoogleplusService;
import de.mobilesol.matebet.web.service.googleplus.dto.GoogleplusAccessTokenDTO;
import de.mobilesol.matebet.web.service.twitter.TwitterService;
import de.mobilesol.matebet.web.service.twitter.dto.TwitterAccessTokenDTO;
import de.mobilesol.matebet.ws.dto.UserProfileDTO;

public class UserEndpoint {

	private static final long serialVersionUID = 1L;

	private static final Logger log = Logger
			.getLogger(UserEndpoint.class.getName());

	private UserService userService = new UserService();

	public UserDTO handleGetUser(int id, HttpServletResponse resp)
			throws IOException {
		UserDTO u = userService.getUser(id, false);
		if (u == null) {
			throw new ResourceNotFoundException(800,
					"User with id " + id + " not found");
		}
		return u;
	}

	public List<UserDTO> handleGetAllUsers(HttpServletResponse resp,
			String query) throws IOException {
		List<UserDTO> users = userService.getAllUsers(query);
		return users;
	}

	public UserDTO handleLogin(String name, String password,
			HttpServletResponse resp) throws IOException {

		UserDTO u = userService.login(name, password);
		if (u == null) {
			throw new AuthException(MatebetException.EXC_LOGIN_FAILED,
					"User with name/email " + name
							+ " not found or wrong password.");
		}
		u.created = null;
		return u;
	}

	public UserDTO handleLoginFb(FacebookAccessTokenDTO token,
			HttpServletResponse resp) throws IOException {

		log.info("handleFbLogin(" + token.access_token + ")");
		UserProfileDTO profile = new FacebookService()
				.readProfile(token.access_token);
		log.info("profile=" + profile);

		UserDTO u = userService.loginExtProvider(profile);
		if (u == null) {
			throw new AuthException(MatebetException.EXC_LOGIN_FAILED,
					"User with name/email " + profile.name
							+ " not found or wrong password.");
		}

		// now add the facebook friends to matebet friends
		List<UserDTO> friends = new FacebookService()
				.getFriends(token.access_token);

		addFriends(u.userId, friends);

		u.created = null;
		return u;
	}

	public UserDTO handleLoginGp(GoogleplusAccessTokenDTO token,
			HttpServletResponse resp) throws IOException {

		UserProfileDTO profile = new GoogleplusService()
				.readProfile(token.access_token);
		log.info("profile=" + profile);

		UserDTO u = userService.loginExtProvider(profile);
		if (u == null) {
			throw new AuthException(MatebetException.EXC_LOGIN_FAILED,
					"User with name/email " + profile.name
							+ " not found or wrong password.");
		}

		// now add google friends to matebet
		List<UserDTO> friends = new GoogleplusService()
				.getFriends(token.access_token);
		addFriends(u.userId, friends);

		u.created = null;
		return u;
	}

	public UserDTO handleLoginTwitter(TwitterAccessTokenDTO token,
			HttpServletResponse resp) throws IOException {

		log.info("handleTwitterLogin(" + token + ")");
		UserProfileDTO profile = new TwitterService().readProfile(token);
		log.info("profile=" + profile);

		UserDTO u = userService.loginExtProvider(profile);
		if (u == null) {
			throw new AuthException(MatebetException.EXC_LOGIN_FAILED,
					"User with name/email " + profile.name
							+ " not found or wrong password.");
		}

		// now add twitter friends to matebet friends
		// adding freinds is skipped List<UserDTO> friends = new
		// TwitterService() .getFriends(token.oauth_token,
		// token.oauth_token_secret);
		//
		// addFriends(u.userId, friends);

		u.created = null;
		return u;
	}

	private void addFriends(int userId, List<UserDTO> friends) {
		log.info("addFriends " + friends);
		for (UserDTO f : friends) {
			new FriendService().addFriend(userId, f.userId, false);
			new FriendService().addFriend(f.userId, userId, false);
		}
	}

	public UserDTO handleUpdate(UserDTO user, HttpServletResponse resp)
			throws IOException {

		UserDTO u = userService.updateUser(user, true);
		if (u == null) {
			throw new ResourceNotFoundException(801,
					"User " + user.name + " does not exist.");
		}

		u.created = null;
		return u;
	}

	public UserDTO handleForgot(String email, HttpServletResponse resp)
			throws IOException {

		UserDTO u = userService.forgotUser(email);
		return u;
	}

	public UserDTO handleReset(int id, String code, HttpServletResponse resp)
			throws IOException {

		UserDTO u = userService.resetUser(id, code);
		if (u == null) {
			throw new ResourceNotFoundException(801,
					"User " + id + "/" + code + " does not exist.");
		}

		return u;
	}

	public Object handleAddUser(Reader in, HttpServletResponse resp)
			throws IOException {

		Gson gson = new Gson();
		UserDTO u = gson.fromJson(in, UserDTO.class);
		u = userService.addUser(u);
		return u;
	}

	public Object handleDeleteUser(int id, HttpServletResponse resp)
			throws IOException {

		userService.deleteUser(id);
		return null;
	}
}
