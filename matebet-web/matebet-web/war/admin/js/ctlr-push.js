var pControllers = angular.module('pushControllers', []);

pControllers.controller('PushCtrl', [ '$scope', '$http',
		function($scope, $http, $routeParams) {

			console.log('send push');
			$scope.form = {};

			$scope.progressloading = false;

		    $scope.doSend = function() {

				$scope.progressloading = true;
				$http({
						url : '/rs/push/send',
						data: $scope.form,
						method : 'POST'
				}).success(function(data) {
						$scope.progressloading = false;
		
					}).error(function(data, status, headers, config) {
						$scope.progressloading = false;
					});
			    }
		
			$scope.doReSend = function() {
			
				$scope.result_resend = '';
				$scope.progressloading = true;
				$http({
						url : '/rs/push/android/resend',
						data: $scope.form,
						method : 'POST'
				}).success(function(data) {
						$scope.progressloading = false;
						$scope.result_resend = "Ok: " + JSON.stringify(data);
					}).error(function(data, status, headers, config) {
						$scope.progressloading = false;
						$scope.result_resend = "Error: " + JSON.stringify(data);
					});
			}

	}
]);
