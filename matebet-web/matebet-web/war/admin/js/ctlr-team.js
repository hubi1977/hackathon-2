var pControllers = angular.module('teamControllers', []);

pControllers.controller('TeamCtrl', ['$scope', '$http',
   function($scope, $http, $routeParams) {

    	console.log('Tshow teams');
       $scope.progressloading = true;
       $http({
           url: '/rs/team/list',
           method: 'GET'
           }).success(function(data) {
               $scope.progressloading = false;
               $scope.teams = data;

           }).error(function(data, status, headers, config) {
               $scope.progressloading = false;
           });
       }
   ]);

