# https://de.wikipedia.org/wiki/Liste_der_Nationalflaggen
# http://www.bundesliga.de/de/liga/tabelle/

rm -fr /tmp/peter
mkdir /tmp/peter

cd peter
for a in `ls icon*.png` ; do
        convert -trim -geometry 54x54 -quality 90 $a /tmp/peter/$a ;
done

cd /tmp/peter
mogrify -crop 50x50+2+2 +repage *.png

cp /tmp/peter/*.png ~/git/matebet/matebet-web/matebet-web/war/images/small

